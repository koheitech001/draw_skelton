import os, sys

from fastapi import APIRouter, Request, File, UploadFile, File
from typing import List, Optional

from controllers.mediapipe import*

router = APIRouter(prefix="")

@router.post('/skelton_video/')
async def post_skelton_video(file_video: UploadFile = File(...), \
                             file_coco: UploadFile = File(...)):
    return await post_skelton_video_(file_video, file_coco)

@router.get('/skelton_video/')
def get_skelton_video(fname: Optional[str] = None):
    return get_skelton_video_(fname)
