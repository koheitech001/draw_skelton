FROM jjanzic/docker-python3-opencv

WORKDIR /myapp
COPY ./ ./
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

EXPOSE 8080
CMD ["python", "./main.py"]